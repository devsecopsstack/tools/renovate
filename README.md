# Renovate for _to be continuous_

This project takes care of maintaining _to be continuous_ projects dependencies up-to-date with [Renovate](https://docs.renovatebot.com/).

It is configured (on gitlab.com) as a scheduled CI/CD job to regularly scan through all _to be continuous_ projects 
dependencies and automatically detect new released versions and propose upgrading those with simple Merge Requests.

## :warning: for self-managed _to be continuous_

If you're using a synchronised copy of _to be continuous_ in a self-managed GitLab, you should not configure scheduled pipeline for this project.

That should only remain in the master project on gitlab.com.
